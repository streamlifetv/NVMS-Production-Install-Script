#!/bin/bash
## Download all required software that is not in PPA ##

echo Downloading Software....
wget https://github.com/angryip/ipscan/releases/download/3.5.1/ipscan_3.5.1_amd64.deb -nc
wget https://www.vpn.net/installers/logmein-hamachi_2.1.0.174-1_amd64.deb -nc
wget http://exacq.com/reseller/8.6/exacqVisionServer-8.6.2.115651_x64.deb --user=guest --password=exacqvisionip --output-document=exacqVisionServerRC.deb -nc
wget http://cdnpublic.exacq.com/8.6/exacqVisionClient-8.6.1.115131_x64.deb --output-document=exacqVisionClientRC.deb -nc
wget http://exacq.com/reseller/8.6/exacqVisionWebService-8.6.3.116175_x64.deb --user=guest --password=exacqvisionip --output-document=exacqVisionWebServiceRC.deb -nc

## Apt-get install standard libs ##
apt-get update
apt-get install gdebi htop openssh-server openssh-client -y

## Use gdebi to install software ##

echo If there is no activity please press TAB followed by ENTER
gdebi exacqVisionServerRC.deb -n
gdebi exacqVisionClientRC.deb -n
gdebi exacqVisionWebService.deb -n
gdebi ipscan_3.5.1_amd64.deb -n
gdebi logmein-hamachi_2.1.0.174-1_amd64.deb -n

## Clean Up Dependencies ##

echo Cleaning up after installation...
apt-get install -f -y

## Setup LogMeIn VPN ##

echo Configuring VPN Connection...
hamachi check-update
hamachi login
hamachi join 298-320-553
## Completed ##

echo System Configured
